package main

import (
	"strings"
	"testing"

	"bitbucket.org/homemade/cephlapod/cephtest"

	"github.com/stretchr/testify/require"
	"github.com/yourheropaul/inj"
)

func assertGraph(t *testing.T, grapher inj.Grapher, input interface{}) {

	require.Nil(t, inj.Provide(input))

	if valid, errors := grapher.Assert(); !valid {
		t.Fatalf(strings.Join(errors, ","))
	}
}

func Test_APluginCanBeBeSetUpAndExecuted(t *testing.T) {

	grapher := inj.NewGraph()

	server, err := cephtest.NewTestServer()
	require.Nil(t, err)

	require.Nil(t, grapher.Provide(
		server,
		server.NewDataModelQuery,
	))

	app, err := setupApplication(grapher)
	require.Nil(t, err)

	go app.Server.Serve()
	defer app.Server.Stop()
}
