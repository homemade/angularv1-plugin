/// <reference path="../typings/angularjs/angular.d.ts" />
/// <reference path="../typings/angularjs/angular-resource.d.ts" />
var Sepia;
(function (Sepia) {
    var Models;
    (function (Models) {
        "use strict";
        var module = angular.module('sepia.models', ['ngResource', 'angular-jsonrpc-client']);
        var datamodel = angular.injector(["sepia.datamodel"]).get("$datamodel");
        var rootPath = window.Sepia.Config.RootPath();
        var apis = window.Sepia.Config.Apis;
        var validation = window.Sepia.Validation();
        var capitalizeFirstLetter = function (string) {
            return string.charAt(0).toUpperCase() + string.slice(1);
        };
        // Setup default RPC endpoint
        module.config(function (jsonrpcConfigProvider) {
            jsonrpcConfigProvider.set({
                url: rootPath + "rpc/v1"
            });
        });
        // Create a model $resource for each model type
        var newVersion = function (name, factoryName, version) {
            module.factory(factoryName, ['$resource', 'jsonrpc', function ($resource, jsonrpc) {
                    var r = $resource(rootPath + 'api/v' + version + '/' + name + '/:id', { id: '@id' }, {
                        'create': { method: 'POST' },
                        'update': { method: 'PUT' }
                    });
                    var rules = {};
                    var hooks = {};
                    for (var fieldname in datamodel[name].fields) {
                        // Build field name metadata
                        rules[fieldname] = {};
                        for (var rulename in datamodel[name].fields[fieldname].validation_rules) {
                            rules[fieldname][rulename] = validation['new'](rulename, datamodel[name].fields[fieldname].validation_rules[rulename]);
                        }
                        // Construct list of hooks
                        hooks[fieldname] = {};
                        for (var hookname in datamodel[name].fields[fieldname].hooks) {
                            hooks[fieldname][hookname] = [];
                            for (var service in datamodel[name].fields[fieldname].hooks[hookname]) {
                                var cfg = datamodel[name].fields[fieldname].hooks[hookname][service];
                                if (cfg.function) {
                                    hooks[fieldname][hookname].push(service + '.' + cfg.function);
                                }
                                if (cfg.functions) {
                                    for (var fn in cfg.functions) {
                                        hooks[fieldname][hookname].push(service + '.' + cfg.functions[fn]);
                                    }
                                }
                            }
                        }
                    }
                    // Overload save to use a more standard API
                    r.prototype.$save = function () {
                        if (this.id) {
                            return this.$update();
                        }
                        else {
                            return this.$create();
                        }
                    };
                    r.prototype.Name = function () {
                        return name;
                    };
                    r.prototype.ValidateField = function (fieldname, value) {
                        var errors = [];
                        if (!rules[fieldname]) {
                            return ["Field '" + fieldname + "' is not defined for model '" + name + "'"];
                        }
                        for (var vrname in rules[fieldname]) {
                            var ret = rules[fieldname][vrname](value);
                            if (ret) {
                                errors.push(ret);
                            }
                        }
                        if (errors.length > 0) {
                            return errors;
                        }
                        return null;
                    };
                    r.prototype.fieldLabel = function (fieldname) {
                        if (!datamodel[name].fields[fieldname]) {
                            return "";
                        }
                        return datamodel[name].fields[fieldname].label;
                    };
                    r.prototype.fieldType = function (fieldname) {
                        if (!datamodel[name].fields[fieldname]) {
                            return "";
                        }
                        return datamodel[name].fields[fieldname].type;
                    };
                    // Add a low level RPC caller to the model
                    r.prototype.CallService = function (functionName, entity) {
                        return jsonrpc.request(functionName + ".Public", [{
                                entityName: name,
                                entity: entity
                            }]);
                    };
                    r.prototype.ValidationServices = function (fieldName, triggerName) {
                        if (hooks[fieldName] && hooks[fieldName][triggerName]) {
                            return hooks[fieldName][triggerName];
                        }
                        return [];
                    };
                    return r;
                }]);
        };
        for (var name_1 in datamodel) {
            newVersion(name_1, '$' + name_1 + 'Model', 1);
            for (var i = 0; i < apis.length; i++) {
                newVersion(name_1, '$' + name_1 + 'Model' + capitalizeFirstLetter(apis[i]), i);
            }
        }
        // Validation directive
        var ValidationDirective = (function () {
            function ValidationDirective($window) {
                // Directive parameters.
                this.restrict = 'A';
                this.require = 'ngModel';
            }
            // Link function
            ValidationDirective.prototype.link = function (scope, element, attrs, ngModel) {
                var path = attrs.ngModel;
                var parts = path.split(/\./);
                var modelName = parts[0];
                var fieldName = parts[1];
                var formErrsKey = 'sepiaFormErrors';
                var generalErrsKey = 'sepiaGeneralErrors';
                var formMetaKey = '$sepia' + parts[0] + 'form';
                var parentObject = scope[modelName];
                // Assert form error container
                scope[formErrsKey] = scope['sepiaFormErrors'] || {};
                scope[formMetaKey] = scope[formMetaKey] || {};
                scope[generalErrsKey] = scope[generalErrsKey] || [];
                // Store meta data if it hasn't already been stored
                if (!scope[formMetaKey][path]) {
                    scope[formMetaKey][path] = {
                        name: fieldName,
                        ngModel: ngModel
                    };
                }
                // Trigger external service hooks on request
                var trigger = function (triggerName) {
                    var serviceNames = parentObject.ValidationServices(fieldName, triggerName);
                    for (var i = 0; i < serviceNames.length; i++) {
                        parentObject.CallService(serviceNames[i], parentObject).then(function (result) {
                            if (result !== undefined && result != null) {
                                if (result.entityFieldErrors !== undefined && result.entityFieldErrors != null) {
                                    for (var fieldName_1 in result.entityFieldErrors) {
                                        var fieldPath = modelName + '.' + fieldName_1;
                                        scope[formErrsKey][fieldPath] = result.entityFieldErrors[fieldName_1][0];
                                        if (scope[formMetaKey][fieldPath]) {
                                            scope[formMetaKey][fieldPath].ngModel.$setValidity('sepiaValidation', false);
                                        }
                                    }
                                }
                                if (result.errors !== undefined && result.errors != null) {
                                    for (var i_1 = 0; i_1 < result.errors.length; i_1++) {
                                        scope[generalErrsKey].push(result.errors[i_1]);
                                    }
                                }
                            }
                        })
                            .catch(function (error) {
                            scope[generalErrsKey].push(error.error);
                        });
                    }
                };
                // Watch for any changes
                scope.$watch(attrs.ngModel, function (value) {
                    var errors = parentObject.ValidateField(parts[1], value);
                    if (errors) {
                        ngModel.$setValidity('sepiaValidation', false);
                        scope[formErrsKey][path] = errors[0];
                    }
                    else {
                        ngModel.$setValidity('sepiaValidation', true);
                        delete scope[formErrsKey][path];
                        // Trigger: after validation (FIXME: get proper service name) 
                        trigger('ui:after-validation');
                    }
                });
                // Run any triggers on blur
                element.bind('blur', function () {
                    trigger('ui:on-blur');
                });
            };
            // Creates an instance of the compareToDirective class.
            ValidationDirective.factory = function () {
                var directive = function ($window) { return new ValidationDirective($window); };
                directive.$inject = ['$window'];
                return directive;
            };
            // Constructor 
            ValidationDirective.$inject = ['$window'];
            return ValidationDirective;
        })();
        module.directive('sepiaValidate', ValidationDirective.factory());
    })(Models = Sepia.Models || (Sepia.Models = {}));
})(Sepia || (Sepia = {}));
