package main

import (
	"bytes"
	"fmt"
	"html/template"
	"net/http"
	"path/filepath"
	"strings"
	"time"

	"github.com/yosssi/ace"

	"bitbucket.org/homemade/cephlapod"
)

const (
	configTemplateModuleName = "template_module_name"
	configTemplatePaths      = "template_paths"
	defaultModuleName        = "sepia.templates"
)

type dataModelQueryFn func() cephlapod.DataModelQuery

type application struct {
	Server             cephlapod.Server `inj:""`
	NewDataModelQuery  dataModelQueryFn `inj:""`
	lastUpdated        time.Time
	datamodel          javascript
	views              javascript
	templateModuleName string
	watchPaths         []string
	scriptPath         string
}

func newApplication() *application {
	return &application{}
}

func (a *application) validateConfig(ctx cephlapod.Context, logger cephlapod.Logger, cfg cephlapod.ConfigMap) error {
	// Module name
	a.templateModuleName = defaultModuleName

	if moduleName, ok := cfg[configTemplateModuleName]; ok {
		a.templateModuleName = moduleName
	}

	logger.Log("Using '%s' for template module name", a.templateModuleName)

	// Reset old paths
	if len(a.watchPaths) > 0 {
		a.Server.FileUnwatch(a.watchPaths...)
	}

	// Store any new paths
	if paths, ok := cfg[configTemplatePaths]; ok {
		a.watchPaths = strings.Split(paths, ",")
	}

	logger.Log("Watching template paths %s", a.watchPaths)

	// Store the date
	a.lastUpdated = time.Now()

	// The main script HTTP handler
	a.scriptPath = fmt.Sprintf("/plugins/%s/angular.js", ctx.Handle)
	a.Server.URL(a.scriptPath, a.scriptHandler)

	// Tell the user what's going on
	logger.Log("Serving javascript at %s (relative to root path)", a.scriptPath)

	// Make sure all the paths are watched
	a.Server.FileWatch(a.frontendTemplateChanged, a.templateGlobPaths()...)

	return nil
}

func (a *application) ready(ctx cephlapod.Context, logger cephlapod.Logger) {

	// Store the date
	a.lastUpdated = time.Now()

	// Get the new data model
	dataModel, err := a.NewDataModelQuery().Execute()

	if err != nil {
		logger.Log("Failed to get data model: %s", err)
		return
	}

	// Store the datamodel
	a.datamodel = newDatamodelJavascript(dataModel)

	// Glob any files and trigger the first build
	files, err := a.Server.FileGlob(a.templateGlobPaths()...)

	if err != nil {
		logger.Log("Failed to glob template paths: %s", err)
		return
	}

	a.rebuildFrontendTemplates(logger, files)
}

func (a *application) templateGlobPaths() (globs []string) {

	for _, v := range a.watchPaths {
		globs = append(globs, filepath.Join(v, "/*.ace"), filepath.Join(v, "/**/*.ace"))
	}

	return
}

func (a *application) templatePublicPath(input string) (output string) {

	output = input

	for _, path := range a.watchPaths {

		p := strings.TrimPrefix(output, strings.TrimRight(path, "/")+"/")

		if len(p) != len(output) {
			return p
		}
	}

	return
}

func (a *application) scriptHandler(ctx cephlapod.Context, r *http.Request, w http.ResponseWriter) error {

	rs := newJavascriptReadSeeker(func() javascript {
		return newMinifier(
			newBinaryJavascript("angular.min.js"),
			newBinaryJavascript("angular-route.min.js"),
			newBinaryJavascript("angular-resource.min.js"),
			newBinaryJavascript("angular-jsonrpc-client.js"),
			a.views,
			a.datamodel,
			newBinaryJavascript("sepia.models.js"),
		)
	})

	http.ServeContent(w, r, "angular.js", a.lastUpdated, rs)

	return nil
}

func (a *application) frontendTemplateChanged(logger cephlapod.Logger, files []string, change cephlapod.FileChange, path string) {
	logger.Log("%s %s, building template cache...", path, change)
	a.rebuildFrontendTemplates(logger, files)
	logger.Log("template cache build complete")
}

func (a *application) rebuildFrontendTemplates(logger cephlapod.Logger, files []string) {

	views := make(map[string]string)

	for _, file := range files {

		content, err := a.Server.FileRead(file)

		if err != nil {
			logger.Log("[ERROR] Failed to read '%s': %s", file, err)
			return
		}

		logger.Log("Processing %s", file)

		url, html, err := a.templateHTML(a.templatePublicPath(file), content)

		if err != nil {
			logger.Log("[ERROR] Failed to parse ACE template: %s", err)
			return
		}

		views[url] = string(html)
	}

	a.views = newFrontendTemplateJavascript(a.templateModuleName, views)

	// Force script reload
	a.lastUpdated = time.Now()

	// Trigger a live reload
	if err := a.Server.LiveReload(a.scriptPath); err != nil {
		logger.Log("[ERROR] Failed to trigger live reload: %s", err)
		return
	}
}

func (a *application) templateHTML(url string, input []byte) (finalUrl, output string, err error) {

	// Parse the template
	source := ace.NewSource(
		ace.NewFile("", nil),
		ace.NewFile(url, input),
		[]*ace.File{},
	)

	options := ace.Options{
		//Because we use {{ and }} for angular.js
		DelimLeft:  "<<",
		DelimRight: ">>",
	}

	r, err := ace.ParseSource(source, &options)

	if err != nil {
		return "", "", err
	}

	tmpl, err := ace.CompileResultWithTemplate(template.New(url), r, &options)
	if err != nil {
		return "", "", err
	}

	buf := bytes.Buffer{}

	if err := tmpl.Execute(&buf, struct{}{}); err != nil {
		return "", "", err
	}

	output = buf.String()

	// Set the final URL
	finalUrl = strings.TrimSuffix(url, ".ace") + ".html"

	return
}
