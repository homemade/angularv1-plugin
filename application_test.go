package main

import (
	"fmt"
	"testing"

	"bitbucket.org/homemade/cephlapod"
	"bitbucket.org/homemade/cephlapod/cephtest"
	"bitbucket.org/homemade/cephlapod/cephtest/iqs"
	"bitbucket.org/homemade/cephlapod/pb"
	"github.com/Pallinder/go-randomdata"
	"github.com/stretchr/testify/require"
)

func mockApplication(t *testing.T) (app *application, handle string) {
	return newApplication(), randomdata.SillyName()
}

func mockContext(t *testing.T, handle string) cephlapod.Context {
	return cephlapod.Context{
		Port:   ":1234",
		Handle: handle,
	}
}

func mockContextWithConfig(t *testing.T, handle string, config cephlapod.ConfigMap) cephlapod.Context {
	return cephlapod.Context{
		Port:   ":1234",
		Handle: handle,
		Config: config,
	}
}

func mockConfig(t *testing.T) cephlapod.ConfigMap {
	return cephlapod.ConfigMap{
		configTemplateModuleName: "my.module",
		configTemplatePaths:      "/path/to/templates,relative/path/to/teplates",
	}
}

func mockIQSWithFileSystem(t *testing.T) (iqs.GrpcServer, string, func() error) {

	qs, addr := iqs.NewGrpcServer(t)

	qs.FileSystem(map[string][]byte{
		"ace/b.ace": []byte("content B"),
		"ace/a.ace": []byte("content A"),
		"ace/c.ace": []byte("content C"),
	})

	return qs, addr, qs.Stop
}

func Test_AnApplicationCanValidateConfigOverTheWire(t *testing.T) {

	app, handle := mockApplication(t)

	server, err := cephtest.NewTestServer()
	require.Nil(t, err)
	defer server.Stop()
	app.Server = server

	for cycle, test := range []struct {
		description string
		config      cephlapod.ConfigMap
		moduleName  string
		watchPaths  []string
	}{
		{
			description: "Default config",
			config:      cephlapod.ConfigMap{},
			moduleName:  "sepia.templates",
		},
		{
			description: "Custom config",
			config:      mockConfig(t),
			moduleName:  "my.module",
			watchPaths:  []string{"/path/to/templates", "relative/path/to/teplates"},
		},
	} {

		t.Logf("Cycle %d: %s", cycle, test.description)

		ctx, log := mockContextWithConfig(t, handle, test.config), cephtest.NewLogger(t)

		require.Nil(t, app.validateConfig(ctx, log, test.config))

		require.Equal(t, test.moduleName, app.templateModuleName)
		require.Equal(t, test.watchPaths, app.watchPaths)

		log.Assert(
			fmt.Sprintf("Using '%s' for template module name", app.templateModuleName),
			fmt.Sprintf("Watching template paths %s", app.watchPaths),
			fmt.Sprintf("Serving javascript at /plugins/%s/angular.js (relative to root path)", handle),
		)
	}
}

func Test_AnApplicationCanReturnGlobPathsBasedOnWatchPaths(t *testing.T) {

	for cycle, test := range []struct {
		description string
		watchPaths  []string
		globs       []string
	}{
		{
			description: "Empty watch paths",
		},
		{
			description: "Non-empty watch paths",
			watchPaths:  []string{"path/to/ace/"},
			globs:       []string{"path/to/ace/*.ace", "path/to/ace/**/*.ace"},
		},
	} {

		t.Logf("Cycle %d: %s", cycle, test.description)

		app, _ := mockApplication(t)
		app.watchPaths = test.watchPaths
		require.Equal(t, test.globs, app.templateGlobPaths())
	}
}

func Test_AnApplicationCanTrimPublicFilesBasedOnWatchPaths(t *testing.T) {

	for cycle, test := range []struct {
		description string
		watchPaths  []string
		input       string
		output      string
	}{
		{
			description: "Empty watch paths",
		},
		{
			description: "Non-empty watch paths",
			watchPaths:  []string{"path/to/ace/"},
			input:       "path/to/ace/subdir/file.ace",
			output:      "subdir/file.ace",
		},
		{
			description: "Non-empty watch paths: trailing slash",
			watchPaths:  []string{"path/to/ace/"},
			input:       "path/to/ace/subdir/file.ace",
			output:      "subdir/file.ace",
		},
	} {

		t.Logf("Cycle %d: %s", cycle, test.description)

		app, _ := mockApplication(t)
		app.watchPaths = test.watchPaths
		require.Equal(t, test.output, app.templatePublicPath(test.input))
	}
}

func Test_AnApplicationCanProcessTemplateInput(t *testing.T) {

	for cycle, test := range []struct {
		description string
		url         string
		input       string
		finalUrl    string
		output      string
		err         error
	}{
		{
			description: "basic ACE transform",
			url:         "test.ace",
			input: `
.row
  p Hello!
`,
			finalUrl: "test.html",
			output:   "<div class=\"row\"><p>Hello!</p></div>",
		},
		{
			description: "failed ACE transform",
			url:         "test.ace",
			input: `
.row
 p Hello!
  <<p Hello!
`,
			err: fmt.Errorf("template: test.ace:1: unexpected \"<\" in command"),
		},
	} {
		t.Logf("Cycle %d: %s", cycle, test.description)

		finalUrl, output, err := newApplication().templateHTML(test.url, []byte(test.input))

		if test.err == nil {
			require.Nil(t, err)
			require.Equal(t, test.finalUrl, finalUrl)
			require.Equal(t, test.output, output)
		} else {
			require.NotNil(t, err)
			require.Equal(t, test.err.Error(), err.Error())
		}
	}
}

func Test_AnApplicationCanProcessIQSQueriesWhenReady(t *testing.T) {

	app, handle := mockApplication(t)

	ctx, log := mockContext(t, handle), cephtest.NewLogger(t)

	server, err := cephtest.NewTestServer()
	require.Nil(t, err)
	defer server.Stop()

	qs, _, stopQs := mockIQSWithFileSystem(t)
	defer stopQs()

	require.Nil(t, qs.Attach(server.(pb.SetupServer)))

	app.Server = server

	app.NewDataModelQuery = func() cephlapod.DataModelQuery {
		return cephtest.NewMockDataModelQuery(func() (map[string]cephlapod.ModelDefinition, error) {
			return map[string]cephlapod.ModelDefinition{
				"A": cephlapod.ModelDefinition{
					Fields: map[string]*cephlapod.ModelFieldDefinition{
						"field0": &cephlapod.ModelFieldDefinition{
							Label: "label0",
							Validation: map[string]cephlapod.ModelFieldValidationDefinition{
								"rule0": cephlapod.ModelFieldValidationDefinition{
									"one": "two",
								},
							},
							Hooks: map[string]cephlapod.ModelHookDefinition{
								"ui:on-blur": cephlapod.ModelHookDefinition{
									"service0": &cephlapod.ModelHookActionDefinition{
										Functions: []string{"fn0"},
										Async:     true,
										Priority:  999,
									},
								},
							},
						},
					},
				},
				"B": cephlapod.ModelDefinition{
					Fields: map[string]*cephlapod.ModelFieldDefinition{
						"field0": &cephlapod.ModelFieldDefinition{
							Label: "label0",
							Validation: map[string]cephlapod.ModelFieldValidationDefinition{
								"rule0": cephlapod.ModelFieldValidationDefinition{
									"one": "two",
								},
							},
							Hooks: map[string]cephlapod.ModelHookDefinition{
								"ui:on-blur": cephlapod.ModelHookDefinition{
									"service0": &cephlapod.ModelHookActionDefinition{
										Functions: []string{"fn0"},
										Async:     true,
										Priority:  999,
									},
								},
							},
						},
					},
				},
			}, nil
		})
	}

	app.ready(ctx, log)

	log.Assert(
		"Processing ace/a.ace",
		"Processing ace/b.ace",
		"Processing ace/c.ace",
	)

	// Make sure that the frontend JS has been processed
	/*	frontendJs, err := app.views.Content()
		require.Nil(t, err)
		fmt.Println(string(frontendJs))*/
}
