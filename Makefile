PLUGIN_NAME    ?= angularv1
PLUGIN_ROOT    ?= ../..
PLUGIN_HOME    ?= .
PLUGIN_VERSION ?= 1.5.5

$(PLUGIN_HOME)/$(PLUGIN_NAME)-$(PLUGIN_VERSION):
	@go test
	@go-bindata -o binary_data.go -prefix lib lib/...
	@cd ts; tsc
	@go build -o $(PLUGIN_HOME)/$(PLUGIN_NAME)-$(PLUGIN_VERSION) *.go

install: typescript binarydata test
	@go get -v -t ../...	
	@go build -o $(PLUGIN_HOME)/$(PLUGIN_NAME)-$(PLUGIN_VERSION) *.go

test:
	@go test

typescript:
	@cd ts; tsc

typescript-watch:
	@cd ts; tsc -w

binarydata:
	@go-bindata -o binary_data.go -prefix lib lib/...

clean:
	-@rm $(PLUGIN_HOME)/$(PLUGIN_NAME)-$(PLUGIN_VERSION)
