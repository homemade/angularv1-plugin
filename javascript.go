package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	html_template "html/template"
	"io"
	"text/template"

	"github.com/tdewolff/minify"
	"github.com/tdewolff/minify/js"

	"bitbucket.org/homemade/cephlapod"
)

type javascript interface {
	Content() ([]byte, error)
}

/////////////////////////////////////////////////////////////////////
// Minifier
/////////////////////////////////////////////////////////////////////

type minifier struct {
	scripts []javascript
}

func newMinifier(js ...javascript) javascript {
	return &minifier{
		scripts: js,
	}
}

func (m *minifier) Content() ([]byte, error) {

	var compiled []byte

	for _, input := range m.scripts {

		if input == nil {
			continue
		}

		script, err := input.Content()

		if err != nil {
			return []byte{}, err
		}

		compiled = append(compiled, script...)
	}

	final := new(bytes.Buffer)
	min := minify.New()
	min.AddFunc("application/javascript", js.Minify)
	if err := min.Minify("application/javascript", final, bytes.NewBuffer(compiled)); err != nil {
		return []byte{}, err
	}

	return final.Bytes(), nil
}

/////////////////////////////////////////////////////////////////////
// Data model
/////////////////////////////////////////////////////////////////////

type datamodelJavascript struct {
	dm map[string]cephlapod.ModelDefinition
}

func newDatamodelJavascript(dm map[string]cephlapod.ModelDefinition) javascript {
	return &datamodelJavascript{
		dm: dm,
	}
}

func (d *datamodelJavascript) Content() ([]byte, error) {
	template := template.Must(template.New("").
		Funcs(
			template.FuncMap{
				"json": func(inp interface{}) html_template.JS {

					jsn, err := json.Marshal(inp)

					if err == nil {
						return html_template.JS(jsn)
					}

					return html_template.JS(fmt.Sprintf("{\"json_encode_failed\":\"%s\"}", err))
				},
			},
		).
		Parse(
			`
angular.module("sepia.datamodel",[]).factory('$datamodel', function() {
	return {{.|json}};
});
		 `,
		))

	buff := new(bytes.Buffer)

	err := template.Execute(
		buff,
		d.dm,
	)

	if err != nil {
		return []byte{}, err
	}

	return buff.Bytes(), nil
}

/////////////////////////////////////////////////////////////////////
// Binary data files
/////////////////////////////////////////////////////////////////////

type binaryJavascript struct {
	path string
}

func newBinaryJavascript(path string) javascript {
	return &binaryJavascript{
		path: path,
	}
}

func (d *binaryJavascript) Content() ([]byte, error) {
	return Asset(d.path)
}

/////////////////////////////////////////////////////////////////////
// Frontend template compiler
/////////////////////////////////////////////////////////////////////

var cacheTemplate = template.Must(template.New("").Parse(
	`angular.module("{{ .ModuleName }}",[]).run(["$templateCache",
		function($templateCache) {
			{{ range $path, $file := .Views }}
				$templateCache.put("{{ $path }}", "{{ $file | js }}");
			{{ end }}
		}
	])`,
))

type frontendTemplateJavascipt struct {
	ModuleName string
	Views      map[string]string
}

func newFrontendTemplateJavascript(moduleName string, views map[string]string) javascript {
	return &frontendTemplateJavascipt{
		ModuleName: moduleName,
		Views:      views,
	}
}

func (j *frontendTemplateJavascipt) Content() ([]byte, error) {

	buf := bytes.Buffer{}

	if err := cacheTemplate.Execute(&buf, j); err != nil {
		return []byte{}, err
	}

	return buf.Bytes(), nil
}

/////////////////////////////////////////////////////////////////////
// Readseeker
/////////////////////////////////////////////////////////////////////

type javascriptReadSeeker struct {
	fn  func() javascript
	buf *bytes.Reader
}

func newJavascriptReadSeeker(fn func() javascript) io.ReadSeeker {

	drs := javascriptReadSeeker{}
	drs.fn = fn

	return &drs
}

func (l *javascriptReadSeeker) assert() error {

	if l.buf == nil {

		c, err := l.fn().Content()

		if err != nil {
			return err
		}

		l.buf = bytes.NewReader(c)
	}

	return nil
}

func (l *javascriptReadSeeker) Read(p []byte) (n int, err error) {

	if err := l.assert(); err != nil {
		return 0, err
	}

	return l.buf.Read(p)
}

func (l *javascriptReadSeeker) Seek(offset int64, whence int) (int64, error) {

	if err := l.assert(); err != nil {
		return 0, err
	}

	return l.buf.Seek(offset, whence)
}
