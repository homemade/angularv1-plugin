package main

import (
	"fmt"
	"log"
	"strings"

	"github.com/yourheropaul/inj"

	"bitbucket.org/homemade/cephlapod"
)

func main() {

	grapher := inj.NewGraph()

	if err := setupPlugin(grapher); err != nil {
		log.Fatal(err.Error())
	}

	app, err := setupApplication(grapher)

	if err != nil {
		log.Fatalf("Can't set up application: %s", err)
	}

	app.Server.Serve()
}

func setupPlugin(grapher inj.Grapher) error {

	server, err := cephlapod.NewGrpcServer()

	if err != nil {
		return err
	}

	return grapher.Provide(
		server,
		server.NewDataModelQuery,
	)
}

func setupApplication(grapher inj.Grapher) (*application, error) {

	app := newApplication()

	grapher.Provide(app)

	if valid, errors := grapher.Assert(); !valid {
		return nil, fmt.Errorf("Can't assert dependency graph: %s", strings.Join(errors, ","))
	}

	// Handle any config
	app.Server.ValidateConfig(app.validateConfig)

	// Register a callback for when IQS is ready
	app.Server.QueryEngineReady(app.ready)

	return app, nil
}
