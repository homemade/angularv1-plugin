/// <reference path="../typings/angularjs/angular.d.ts" />
/// <reference path="../typings/angularjs/angular-resource.d.ts" />

module Sepia.Models {
    "use strict";

    let module = angular.module('sepia.models', ['ngResource', 'angular-jsonrpc-client']);

    interface IModel extends ng.resource.IResourceClass<ng.resource.IResource<IModel>> {
        Name: () => string;
        ValidateField: (fieldname: string, value: any) => Array<any>;
        CallService: (functionName: string, entity: any) => ng.IPromise<any>;
        ValidationServices: (fieldName: string, triggerName: string) => Array<string>;
    }

    interface IInnerValidationFunction {
        (input: Object): any
    }

    interface IOuterValidationFunction {
        (config: Object): IInnerValidationFunction
    }

    interface IValidationFactory {
        rule: (name: string) => IOuterValidationFunction;
        new: (name: string, config: Object) => IInnerValidationFunction;
    }

    interface IModelValidationRules {
        [index: string]: IInnerValidationFunction;
    }

    interface IModelValidationDescriptor {
        [index: string]: IModelValidationRules;
    }

    interface IModelHooks {
        [index: string]: Array<string>;
    }

    interface IModelHookDescriptor {
        [index: string]: IModelHooks;
    }

    interface IJsonRpc {
        request: (fn: string, arg: any) => ng.IPromise<any>;
    }

    let datamodel: any = angular.injector(["sepia.datamodel"]).get("$datamodel");
    let rootPath: any = (<any>window).Sepia.Config.RootPath();
    let apis: any = (<any>window).Sepia.Config.Apis;
    let validation: IValidationFactory = (<any>window).Sepia.Validation();

    let capitalizeFirstLetter = (string) => {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    // Setup default RPC endpoint
    module.config(function(jsonrpcConfigProvider) {
        jsonrpcConfigProvider.set({
            url: rootPath + "rpc/v1"
        });
    });

    // Create a model $resource for each model type
    let newVersion = (name: string, factoryName: string, version: number) => {

        module.factory(factoryName, ['$resource', 'jsonrpc', ($resource: ng.resource.IResourceService, jsonrpc: IJsonRpc) => {

            let r = $resource(
                rootPath + 'api/v' + version + '/' + name + '/:id',
                { id: '@id' },
                {
                    'create': { method: 'POST' },
                    'update': { method: 'PUT' }
                }
            );

            let rules: IModelValidationDescriptor = {};
            let hooks: IModelHookDescriptor = {};

            for (let fieldname in (<any>datamodel[name].fields)) {

                // Build field name metadata
                rules[fieldname] = <IModelValidationRules>{};

                for (let rulename in (<any>datamodel[name].fields[fieldname].validation_rules)) {
                    rules[fieldname][rulename] = validation['new'](rulename, datamodel[name].fields[fieldname].validation_rules[rulename]);
                }

                // Construct list of hooks
                hooks[fieldname] = <IModelHooks>{};

                for (let hookname in (<any>datamodel[name].fields[fieldname].hooks)) {

                    hooks[fieldname][hookname] = <Array<string>>[];

                    for (let service in (<any>datamodel[name].fields[fieldname].hooks[hookname])) {

                        let cfg = datamodel[name].fields[fieldname].hooks[hookname][service];

                        if (cfg.function) {
                            hooks[fieldname][hookname].push(service + '.' + cfg.function);
                        }

                        if (cfg.functions) {
                            for (let fn in cfg.functions) {
                                hooks[fieldname][hookname].push(service + '.' + cfg.functions[fn]);
                            }
                        }
                    }
                }
            }

            // Overload save to use a more standard API
            r.prototype.$save = function() {
                if (this.id) {
                    return this.$update();
                } else {
                    return this.$create();
                }
            };

            r.prototype.Name = (): string => {
                return name;
            }

            r.prototype.ValidateField = (fieldname: string, value: any): any => {

                let errors: Array<any> = [];

                if (!rules[fieldname]) {
                    return ["Field '" + fieldname + "' is not defined for model '" + name + "'"];
                }

                for (var vrname in rules[fieldname]) {
                    var ret = rules[fieldname][vrname](value);

                    if (ret) { errors.push(ret); }
                }

                if (errors.length > 0) { return errors; }

                return null;
            }

            r.prototype.fieldLabel = (fieldname: string): string => {
                if (!datamodel[name].fields[fieldname]) { return ""; }
                return datamodel[name].fields[fieldname].label;
            }

            r.prototype.fieldType = (fieldname: string): string => {
                if (!datamodel[name].fields[fieldname]) { return ""; }
                return datamodel[name].fields[fieldname].type;
            }

            // Add a low level RPC caller to the model
            r.prototype.CallService = (functionName: string, entity: any): ng.IPromise<any> => {

                return jsonrpc.request(functionName + ".Public", [{
                    entityName: name,
                    entity: entity
                }]);
            }

            r.prototype.ValidationServices = (fieldName: string, triggerName: string): Array<string> => {

                if (hooks[fieldName] && hooks[fieldName][triggerName]) {
                    return hooks[fieldName][triggerName];
                }

                return <Array<string>>[];
            }

            return r;
        }]);
    }

    for (let name in datamodel) {
        newVersion(name, '$' + name + 'Model', 1);

        for (let i: number = 0; i < apis.length; i++) {
            newVersion(name, '$' + name + 'Model' + capitalizeFirstLetter(apis[i]), i);
        }
    }

    // Validation directive
    class ValidationDirective implements angular.IDirective {

        // Directive parameters.
        public restrict = 'A';
        public require = 'ngModel';

        // Constructor 
        public static $inject = ['$window'];
        constructor($window) { }

        // Link function
        public link(scope: angular.IScope, element: angular.IAugmentedJQuery, attrs: angular.IAttributes, ngModel: angular.INgModelController) {

            let path: string = (<any>attrs).ngModel;
            let parts: Array<string> = path.split(/\./);
            let modelName = parts[0];
            let fieldName = parts[1];
            let formErrsKey = 'sepiaFormErrors';
            let generalErrsKey = 'sepiaGeneralErrors';
            let formMetaKey = '$sepia' + parts[0] + 'form';
            let parentObject: IModel = scope[modelName];

            // Assert form error container
            scope[formErrsKey] = scope['sepiaFormErrors'] || {};
            scope[formMetaKey] = scope[formMetaKey] || {};
            scope[generalErrsKey] = scope[generalErrsKey] || <Array<string>>[];

            // Store meta data if it hasn't already been stored
            if (!scope[formMetaKey][path]) {
                scope[formMetaKey][path] = {
                    name: fieldName,
                    ngModel: ngModel
                };
            }

            // Trigger external service hooks on request
            let trigger = (triggerName: string) => {

                let serviceNames = parentObject.ValidationServices(fieldName, triggerName);

                for (let i = 0; i < serviceNames.length; i++) {
                    parentObject.CallService(serviceNames[i], parentObject).then(function(result) {

                        if (result !== undefined && result != null) {

                            if (result.entityFieldErrors !== undefined && result.entityFieldErrors != null) {
                                for (let fieldName in result.entityFieldErrors) {

                                    let fieldPath = modelName + '.' + fieldName;
                                    scope[formErrsKey][fieldPath] = result.entityFieldErrors[fieldName][0];

                                    if (scope[formMetaKey][fieldPath]) {
                                        scope[formMetaKey][fieldPath].ngModel.$setValidity('sepiaValidation', false);
                                    }
                                }
                            }

                            if (result.errors !== undefined && result.errors != null) {
                                for (let i = 0; i < result.errors.length; i++) {
                                    scope[generalErrsKey].push(result.errors[i]);
                                }
                            }
                        }
                    })
                        .catch(function(error) {
                            scope[generalErrsKey].push(error.error);
                        });
                }
            };

            // Watch for any changes
            scope.$watch((<any>attrs).ngModel, function(value) {

                let errors = parentObject.ValidateField(parts[1], value);

                if (errors) {
                    ngModel.$setValidity('sepiaValidation', false);
                    scope[formErrsKey][path] = errors[0];
                } else {
                    ngModel.$setValidity('sepiaValidation', true);
                    delete scope[formErrsKey][path];

                    // Trigger: after validation (FIXME: get proper service name) 
                    trigger('ui:after-validation');
                }
            });

            // Run any triggers on blur
            element.bind('blur', function() {
                trigger('ui:on-blur');
            });
        }

        // Creates an instance of the compareToDirective class.
        public static factory(): angular.IDirectiveFactory {
            const directive = ($window: angular.IWindowService) => new ValidationDirective($window);
            directive.$inject = ['$window'];
            return directive;
        }
    }

    module.directive('sepiaValidate', ValidationDirective.factory());
}
