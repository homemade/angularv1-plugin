package main

import (
	"bytes"
	"testing"

	"github.com/stretchr/testify/require"
	"github.com/tdewolff/minify"
	"github.com/tdewolff/minify/js"
)

func minifyJs(t *testing.T, input []byte) []byte {
	buf := bytes.Buffer{}
	min := minify.New()
	min.AddFunc("application/javascript", js.Minify)
	require.Nil(t, min.Minify("application/javascript", &buf, bytes.NewBuffer(input)))
	return buf.Bytes()
}

func compareMinifiedJs(t *testing.T, js0, js1 []byte) {
	require.Equal(t, string(minifyJs(t, js0)), string(minifyJs(t, js1)))
}

func Test_AFrontendJavaScriptCompilerCanGenerateAnAngularTemplateCache(t *testing.T) {

	js := newFrontendTemplateJavascript("my.module", map[string]string{
		"A": "Hello A",
		"D": "Hello D",
		"C": "Hello C",
	})

	expected := `angular.module("my.module",[]).run(["$templateCache",
                function($templateCache) {
                                $templateCache.put("A", "Hello A");
                                $templateCache.put("C", "Hello C");
                                $templateCache.put("D", "Hello D");
                }
        ])`

	output, err := js.Content()
	require.Nil(t, err)
	compareMinifiedJs(t, []byte(expected), output)
}
