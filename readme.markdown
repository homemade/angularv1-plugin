### Support for Angular 1.5.5, including model $resources and frontend template compilation

Plugin angular provides an AngularJS frontend toolkit for use in _Sepia_ applications. A full
description of how to use the plugin in provided in the [Angular.js plugin](/general.angular) section.

